/**
 * Earth viewer configuration
 */
var viewer = new Cesium.Viewer('cesiumContainer');
var container = document.getElementById('cesiumContainer');
var startDate = Cesium.JulianDate.fromIso8601(container.getAttribute('data-start'));
var endDate = Cesium.JulianDate.fromIso8601(container.getAttribute('data-end'));

viewer.dataSources.add(Cesium.KmlDataSource
    .load('/result', {
        camera: viewer.camera,
        canvas: viewer.canvas
    }))
    .then(function (dataSource) {
        viewer.flyTo(dataSource.entities);
        viewer.timeline.zoomTo(startDate, endDate);
        viewer.clock.startTime = startDate;
        viewer.clock.stopTime = endDate;
        viewer.clock.multiplier = 0.5;
        viewer.clock.clockStep = Cesium.ClockStep.TICK_DEPENDENT;
        viewer.clock.clockRange = ClockRange.LOOP_STOP;
    });

/**
 * Todo: Statistics table
 */

