/*
 * Tahina Ralitera
 * Pick up agent position and time
 * 
 * */

package repastcity.export;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;

import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.util.collections.IndexedIterable;
import repastcity.export.util.AgentManager;
import repastcity3.agent.IAgent;

import com.vividsolutions.jts.geom.Coordinate;

class AgentListener {

	private IndexedIterable<IAgent> agents;

	private HashMap<String, ArrayList<Coordinate>> allPath = new HashMap<>();
	private HashMap<String, ArrayList<Instant>> allInstant = new HashMap<>();

	AgentListener() {
		this.agents = AgentManager.getAllAgents();
	}

    void listen() {
        ISchedule schedule = RunEnvironment.getInstance().getCurrentSchedule();
        // parameter for a call of setAgentPath method every 10 interval
        ScheduleParameters parameters = ScheduleParameters.createRepeating(1, 10, ScheduleParameters.FIRST_PRIORITY);
        schedule.schedule(parameters, this, "setAgentPath");
    }

	public void setAgentPath() {
		for(IAgent agent: this.agents) {
            Coordinate agentCoordinate = AgentManager.coordinateOf(agent);
			setAllAgentPath(agent.toString(), agentCoordinate, Instant.now());
		}
	}

	private void setAllAgentPath(String key, Coordinate spaceCoordinate, Instant timeCoordinate) {
		if(allPath.containsKey(key)){
			updateExisting(key, spaceCoordinate, timeCoordinate);
		} else {
			createNew(key, spaceCoordinate, timeCoordinate);
		}
	}

	private void updateExisting(String key, Coordinate spaceCoordinate, Instant timeCoordinate) {
        ArrayList<Coordinate> spaces = this.allPath.get(key);
        ArrayList<Instant> instants = this.allInstant.get(key);
        spaces.add(spaceCoordinate);
        instants.add(timeCoordinate);
        this.allPath.replace(key, spaces);
        this.allInstant.replace(key, instants);
    }

    private void createNew(String key, Coordinate spaceCoordinate, Instant timeCoordinate) {
        ArrayList<Coordinate> spaces = new ArrayList<>();
        ArrayList<Instant> instants = new ArrayList<>();
        spaces.add(spaceCoordinate);
        instants.add(timeCoordinate);
        this.allPath.put(key, spaces);
        this.allInstant.put(key, instants);
    }

    HashMap<String, ArrayList<Coordinate>> getAgentPath() {
		return allPath;
	}

    HashMap<String, ArrayList<Instant>> getAgentTimeStamp() {
		return allInstant;
	}

}
