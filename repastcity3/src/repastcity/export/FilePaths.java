package repastcity.export;

/**
 * Maintain list of file paths
 */
public class FilePaths {
  private FilePaths() {}

  public static String KMZ_RESULT = "./FileOutput/simulation.kmz";
  public static String CSV_STATISTIC = "./FileOutput/day-0-occupancy.csv"; // Todo: csv file containing statistics
}
