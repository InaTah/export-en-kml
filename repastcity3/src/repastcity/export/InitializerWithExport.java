/* Tahina Ralitera
 * 
 * Listen to any run-related events such as started, stopped, paused ...
 * 
 */
package repastcity.export;

import java.awt.*;
import java.io.FileNotFoundException;
import java.net.URI;
import java.time.Instant;

import repast.simphony.engine.environment.RunEnvironmentBuilder;
import repast.simphony.engine.environment.RunListener;
import repast.simphony.scenario.ModelInitializer;
import repast.simphony.scenario.Scenario;

public class InitializerWithExport implements ModelInitializer {

  private final String RESULT_URL = "http://localhost:9090/simulation";

  private AgentListener listener;
  private Instant startDate;
  private Instant endDate;

  @Override
  public void initialize(Scenario scenario, RunEnvironmentBuilder builder) {
    System.err.println("INITIALIZING!");
    builder.getScheduleRunner().addRunListener(new RunListener() {

      @Override
      public void stopped() {
        endDate = Instant.now();

        try {
          new SimulationExport.Builder(listener)
              .build()
              .process();

          openResultInBrowser();

          System.out.println("EXPORT DONE");
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }

        System.err.println("STOPPED!");
      }

      private void openResultInBrowser() {
        String currentResultUrl = RESULT_URL +
            "?start=" + startDate.toString() +
            "&end=" + endDate.toString();
        try {
          Desktop.getDesktop().browse(new URI(currentResultUrl));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      @Override
      public void started() {
        startDate = Instant.now();
        listener = new AgentListener();
        listener.listen();
        System.err.println("STARTED!");
      }

      @Override
      public void restarted() {
        System.err.println("RESTARTED!");
      }

      @Override
      public void paused() {
        System.err.println("PAUSED!");
      }
    });
  }


}
