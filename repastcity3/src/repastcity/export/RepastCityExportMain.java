package repastcity.export;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import repastcity.export.factories.RouteFactory;
import spark.ModelAndView;
import spark.Route;
import spark.TemplateViewRoute;
import spark.template.mustache.MustacheTemplateEngine;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.staticFileLocation;

/**
 * We need this class to launch the server with the simulation
 *
 */
public class RepastCityExportMain {

  public static void main(String[] args) {
    initServer();
    repast.simphony.runtime.RepastMain.main(args);
  }

  public static void initServer() {
    staticFileLocation("/public");
    port(9090);

    Function<String, TemplateViewRoute> endStartViewRoute = (templateName) -> (request, response) -> {
      Map<String, String> map = new HashMap<>();
      map.put("startDate", request.queryMap("start").value());
      map.put("endDate", request.queryMap("end").value());

      return new ModelAndView(map, templateName);
    };

    /*
     * Simulation Result Page
     */
    TemplateViewRoute resultRoute = endStartViewRoute.apply("result.mustache");

    get("/simulation", resultRoute, new MustacheTemplateEngine());

    /*
     * Viewer frame
     */

    TemplateViewRoute viewRoute = endStartViewRoute.apply("viewer.mustache");

    get("/viewer", viewRoute, new MustacheTemplateEngine());

    /*
     * Retrieve KMZ file
     */
    Route kmzRoute = RouteFactory.createWithFile(FilePaths.KMZ_RESULT);

    get("/result", "application/zip", kmzRoute);


    /*
     * Retrieve CSV statistic file
     * Todo: Make sure that the path to csv file is correctly filled
     */
    Route csvRoute = RouteFactory.createWithFile(FilePaths.CSV_STATISTIC);

    get("/statistic", "application/csv", csvRoute);
  }
}
