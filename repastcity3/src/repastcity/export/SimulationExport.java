/*
    Tahina Ralitera
 */

package repastcity.export;

import com.vividsolutions.jts.geom.Coordinate;

import de.micromata.opengis.kml.v_2_2_0.*;

import repast.simphony.util.collections.IndexedIterable;
import repastcity.export.factories.FeatureFactory;
import repastcity.export.factories.KmlGeometryFactory;
import repastcity.export.factories.LinkFactory;
import repastcity.export.util.AgentManager;
import repastcity.export.util.ColorUtil;
import repastcity.export.util.KML;
import repastcity3.agent.IAgent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class SimulationExport {

  private AgentListener agentListener;

  private List<Coordinate> agentSpaceCoordinates;
  private List<Instant> agentTimeCoordinates;
  private IndexedIterable<IAgent> agents;

  private HashMap<String, ArrayList<Coordinate>> allPath = new HashMap<>();
  private HashMap<String, ArrayList<Instant>> allInstant = new HashMap<>();

  private Kml kmlObject = KmlFactory.createKml();
  private Document documentTag = kmlObject.createAndSetDocument().withName("MyMarkers");

  private final int STEP = 1;

  private SimulationExport(Builder builder) {
    this.agentListener = builder.agentListener;
  }

  void process() throws FileNotFoundException {
    initAgents();

    for (IAgent agent : this.agents) {
      this.exportAgent(agent);
    }

    saveExportFile();
  }

  private void initAgents() {
    this.agents = AgentManager.getAllAgents();
    this.allPath = agentListener.getAgentPath();
    this.allInstant = agentListener.getAgentTimeStamp();
  }

  private void exportAgent(IAgent agent) {
    String agentColor = ColorUtil.generateColor();
    agentSpaceCoordinates = allPath.get(agent.toString());
    agentTimeCoordinates = allInstant.get(agent.toString());
    for (int coordinateIndex = 0; coordinateIndex < agentSpaceCoordinates.size(); coordinateIndex += STEP) {
      export(
          agentSpaceCoordinates,
          agentTimeCoordinates,
          coordinateIndex,
          agentColor,
          agent);
    }
  }

  public void export(List<Coordinate> agentSpaceCoordinates,
                     List<Instant> agentTimeCoordinates,
                     int coordinateIndex,
                     String color,
                     IAgent agent) {
    Coordinate currentCoordinate = agentSpaceCoordinates.get(coordinateIndex);
    Point currentPoint = KmlGeometryFactory.createPoint(KML.newCoordinate(currentCoordinate.x, currentCoordinate.y));
    Instant currentInstant = agentTimeCoordinates.get(coordinateIndex);
    Placemark placemark = FeatureFactory.createPlacemark(currentPoint, agent.toString(), currentInstant);
    LinkFactory.createIcon(placemark, color);
    this.documentTag.addToFeature(placemark);
  }


  private void saveExportFile() {
    try {
      this.kmlObject.marshalAsKmz(FilePaths.KMZ_RESULT);
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("\n Data stored in " + FilePaths.KMZ_RESULT);
  }


  static class Builder {
    private AgentListener agentListener;

    Builder(AgentListener listener) {
      this.agentListener = listener;
    }

    SimulationExport build() {
      return new SimulationExport(this);
    }
  }
}
