package repastcity.export.factories;

import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.KmlFactory;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

import java.time.Instant;

public class FeatureFactory {
    private FeatureFactory() {}

    public static Placemark createPlacemark(Geometry geom, String name, String description) {
        Placemark placemark = KmlFactory.createPlacemark();
        placemark.setGeometry(geom);
        placemark.setName(name);
        placemark.setDescription(description);
        return placemark;
    }

    public static Placemark createPlacemark(Geometry geometry, String description, Instant currentInstant){
        Placemark placemark = KmlFactory.createPlacemark();
        placemark.setGeometry(geometry);
        placemark.setDescription(description);
        placemark.createAndSetTimeStamp().setWhen(currentInstant.toString());
        return placemark;
    }
}
