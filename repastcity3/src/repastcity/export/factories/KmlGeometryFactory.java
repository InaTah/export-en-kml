package repastcity.export.factories;


import de.micromata.opengis.kml.v_2_2_0.*;
import repastcity.export.util.ColorUtil;
import repastcity3.environment.Building;
import repastcity3.main.ContextManager;


public class KmlGeometryFactory {

    private KmlGeometryFactory() {}

    public static Polygon createPolygon(Building bu, Placemark placemark, int height) {

        Polygon poly = KmlFactory.createPolygon();
        LineStyle ls = placemark.createAndAddStyle().createAndSetLineStyle();
        ls.setWidth(1.5);
        PolyStyle pls = placemark.createAndAddStyle().createAndSetPolyStyle();
        pls.setColor(ColorUtil.generateColor());
        poly.setExtrude(true);
        poly.setAltitudeMode(AltitudeMode.RELATIVE_TO_GROUND);
        Boundary bound = new Boundary();
        LinearRing lnr = new LinearRing();

        com.vividsolutions.jts.geom.Coordinate[] coordinates = ContextManager.buildingProjection
                                                                                .getGeometry(bu)
                                                                                .getCoordinates();
        for(com.vividsolutions.jts.geom.Coordinate coord : coordinates){
            Coordinate c=new Coordinate(coord.x,coord.y,height);
            lnr.addToCoordinates(c.toString());
        }

        bound.setLinearRing(lnr);
        poly.setOuterBoundaryIs(bound);

        return poly;
    }

    public static Point createPoint(Coordinate coords){
        Point point = KmlFactory.createPoint();
        point.getCoordinates().add(coords);
        return point;
    }

    //Todo: Create Curve
}
