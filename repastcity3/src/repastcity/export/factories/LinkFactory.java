package repastcity.export.factories;

import de.micromata.opengis.kml.v_2_2_0.*;

public class LinkFactory {

    private static final String ICON_LINK = "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png";

    private LinkFactory() {}

    public static Icon createIcon(Placemark placemark, String color) {
        // IconStyle
        IconStyle ic= placemark.createAndAddStyle().createAndSetIconStyle();
        ic.setHeading(1);
        ic.setScale(0.5);
        ic.setColor(color);
        Vec2 vc=new Vec2();
        vc.setX(0);
        vc.setY(0.5);
        vc.setXunits(Units.FRACTION);
        vc.setYunits(Units.FRACTION);
        ic.setHotSpot(vc);

        //Icon
        Icon icon=ic.createAndSetIcon();
        icon.setRefreshInterval(0.5);
        icon.setViewRefreshTime(0.5);
        icon.setHref(ICON_LINK);

        return icon;
    }
}
