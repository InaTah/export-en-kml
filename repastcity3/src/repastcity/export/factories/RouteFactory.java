package repastcity.export.factories;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import spark.Route;

public class RouteFactory {
  private RouteFactory() {}

  public static Route createWithFile(String path) {

    return (request, response) -> {
      byte[] bytes = Files.readAllBytes(Paths.get(path));
      HttpServletResponse raw = response.raw();
      raw.getOutputStream().write(bytes);
      raw.getOutputStream().flush();
      raw.getOutputStream().close();

      return response.raw();
    };

  }
}
