package repastcity.export.util;

import com.vividsolutions.jts.geom.Coordinate;
import repast.simphony.util.collections.IndexedIterable;
import repastcity3.agent.IAgent;
import repastcity3.main.ContextManager;

public class AgentManager {
    private AgentManager() {}

    public static IndexedIterable<IAgent> getAllAgents() {
        return ContextManager.getAgentContext().getObjects(IAgent.class);
    }

    public static Coordinate coordinateOf(IAgent agent) {
        return ContextManager.getAgentGeography().getGeometry(agent).getCoordinate();
    }
}
