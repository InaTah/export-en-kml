package repastcity.export.util;

import java.awt.*;

public class ColorUtil {
    private ColorUtil() {}

    public static String generateColor() {
        int red = (int) (( Math.random()*255)+1);
        int green = (int) (( Math.random()*255)+1);
        int blue = (int) (( Math.random()*255)+1);
        Color RandomC = new Color(red,green,blue);
        int RandomRGB = (RandomC.getRGB());
        return Integer.toHexString(RandomRGB);
    }
}
