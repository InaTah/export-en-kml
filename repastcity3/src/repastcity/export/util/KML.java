package repastcity.export.util;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;

public class KML {
    private KML() {
    }

    public static Coordinate newCoordinate(double longitude, double latitude) {
        return new Coordinate(longitude, latitude);
    }
}
